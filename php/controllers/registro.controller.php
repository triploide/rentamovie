<?php
if (!isset($CONFIG)) include '../config.php';
include $CONFIG['include'] . 'php/validators/registro.validator.php';
include $CONFIG['include'] . 'php/classes/Usuario.php';

$errors = validarRegistro();

//si hay errores conrto la ejecucion y los devuelvo
if (count($errors)) {
    $_SESSION['errors'] = $errors;
    header('Location: '.$CONFIG['url'].'registro.php');
    exit;
}

$usuario = new Usuario([
    'nombre' => $_POST['nombre'], 
    'email' => $_POST['email'], 
    'password' => password_hash($_POST['password'], PASSWORD_DEFAULT), 
    'edad' => $_POST['edad']
]);

//AVATAR
//var_dump($_FILES['avatar']); exit;
if ($_FILES['avatar']['tmp_name']) {
    //validación del avatar
    if ($error = $usuario->addAvatar($_FILES['avatar'])) {
        $_SESSION['errors'][] = $error;
        header('Location: '.$CONFIG['url'].'registro.php');
        exit;
    }
} else {
    $usuario->avatar = '';
}

$usuario->save();

//login
$_SESSION['login'] = true;
$_SESSION['usuario'] = $usuario->toArray();
header('Location: '.$CONFIG['url'].'perfil.php');
