<?php
class DB {

    private static $conn;

    public static function getConn()
    {
        global $CONFIG;
        if (!self::$conn) {
            $db = new PDO("mysql:host=localhost;dbname={$CONFIG['db_name']}", $CONFIG['db_user'], $CONFIG['db_password']);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$conn = $db;
        }
        return self::$conn;
    }

}
