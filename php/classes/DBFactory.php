<?php
require_once 'MySQLDB.php';

class DBFactory {

    public static $db_type = 'MySQLDB'; //MySQLDB, JSONDB, MONGODB

    public static function getDB()
    {
        return new self::$db_type;
    }

}
