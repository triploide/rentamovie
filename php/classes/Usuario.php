<?php

require_once 'DB.php';
require_once 'Model.php';

class Usuario extends Model {
    public $id;
    public $email;
    public $password;
    public $edad;
    public $nombre;

    public $fillable = ['nombre', 'email', 'password', 'edad', 'avatar'];
    public static $table = 'usuarios';

    public function addAvatar($request)
    {
        global $CONFIG;

        $errores = '';
        $path = $CONFIG['include'].'content/usuarios/';

        $tiposPermitidos = ['png', 'jpg', 'jpeg', 'gif'];

        if ($request["error"] == UPLOAD_ERR_OK) {
            $nombre = $request["name"];
            $archivo = $request["tmp_name"];
            $ext = pathinfo($nombre, PATHINFO_EXTENSION);
            if (!in_array($ext, $tiposPermitidos)) {
                $errores = "La extensión no es válida";
            } else {
                $nombre = str_replace('@', '-', $_POST['email']);
                $nombre = str_replace('.', '-', $nombre);
                $this->avatar = $nombre.'.'.$ext;
                move_uploaded_file($archivo, $path.$nombre.'.'.$ext);
            }
        } else {
            $errores = 'Sucedió un error durante la carga de la imagen';
        }

        return $errores;
    }
}
